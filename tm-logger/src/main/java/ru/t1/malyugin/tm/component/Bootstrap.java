package ru.t1.malyugin.tm.component;

import lombok.NoArgsConstructor;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.api.IPropertyService;
import ru.t1.malyugin.tm.service.PropertyService;

@Component
@NoArgsConstructor
public final class Bootstrap {

    @Autowired
    IPropertyService propertyService;

    private void prepareStartup() {
        BasicConfigurator.configure();
    }

    public void run(@Nullable final String... args) {
        prepareStartup();
    }

}