package ru.t1.malyugin.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.malyugin.tm.api.repository.model.IProjectRepository;
import ru.t1.malyugin.tm.model.Project;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectRepository extends AbstractWBSRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Class<Project> getClazz() {
        return Project.class;
    }

}