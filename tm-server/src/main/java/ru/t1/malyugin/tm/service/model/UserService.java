package ru.t1.malyugin.tm.service.model;

import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.repository.model.IUserRepository;
import ru.t1.malyugin.tm.api.service.model.IUserService;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.exception.field.EmailEmptyException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.LoginEmptyException;
import ru.t1.malyugin.tm.exception.field.PasswordEmptyException;
import ru.t1.malyugin.tm.exception.user.EmailExistException;
import ru.t1.malyugin.tm.exception.user.LoginExistException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.util.HashUtil;

import javax.persistence.EntityManager;

@Service
@NoArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    protected IUserRepository getRepository() {
        return context.getBean(IUserRepository.class);
    }

    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String pass,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        if (StringUtils.isBlank(pass)) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (!StringUtils.isBlank(email) && isEmailExist(email)) throw new EmailExistException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, pass);
        if (passwordHash == null) throw new PasswordEmptyException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        if (!StringUtils.isBlank(email)) user.setEmail(email.trim());
        if (role != null) user.setRole(role);
        add(user);
        return user;
    }

    @Override
    public void lockUser(@Nullable final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        @Nullable final User user = findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUser(@Nullable final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        @Nullable final User user = findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        @Nullable final User user = findOneByLogin(login);
        if (user == null) return;
        remove(user);
    }

    @Override
    public void removeByEmail(@Nullable final String email) {
        if (StringUtils.isBlank(email)) throw new EmailEmptyException();
        @Nullable final User user = findOneByEmail(email);
        if (user == null) return;
        remove(user);
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        @Nullable final User user = findOneById(id.trim());
        if (user == null) throw new UserNotFoundException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        update(user);
    }

    @Override
    public void update(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final User user = findOneById(id.trim());
        if (user == null) throw new UserNotFoundException();
        if (!StringUtils.isBlank(firstName)) user.setFirstName(firstName.trim());
        if (!StringUtils.isBlank(lastName)) user.setLastName(lastName.trim());
        if (!StringUtils.isBlank(middleName)) user.setMiddleName(middleName.trim());
        update(user);
    }

    @Nullable
    @Override
    public User findOneByEmail(@Nullable final String email) {
        if (StringUtils.isBlank(email)) throw new EmailEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @Nullable final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User findOneByLogin(@Nullable final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @Nullable final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmailExist(@Nullable String email) {
        if (StringUtils.isBlank(email)) throw new EmailEmptyException();
        return findOneByEmail(email) != null;
    }

    @Override
    public boolean isLoginExist(@Nullable String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        return findOneByLogin(login) != null;
    }

}