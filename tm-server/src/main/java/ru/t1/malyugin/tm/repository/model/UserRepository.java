package ru.t1.malyugin.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.malyugin.tm.api.repository.model.IUserRepository;
import ru.t1.malyugin.tm.model.User;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public Class<User> getClazz() {
        return User.class;
    }

    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) {
        @Nullable final String jpql = "SELECT m FROM User m WHERE m.login = :login";
        return entityManager.createQuery(jpql, User.class).setParameter("login", login).setMaxResults(1).getResultList().stream().findAny().orElse(null);
    }

    @Nullable
    @Override
    public User findOneByEmail(@NotNull final String email) {
        @Nullable final String jpql = "SELECT m FROM User m WHERE m.email = :email";
        return entityManager.createQuery(jpql, User.class).setParameter("email", email).setMaxResults(1).getResultList().stream().findAny().orElse(null);
    }

}