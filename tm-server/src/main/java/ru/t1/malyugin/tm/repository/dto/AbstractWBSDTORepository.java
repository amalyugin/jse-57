package ru.t1.malyugin.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.malyugin.tm.api.repository.dto.IWBSDTORepository;
import ru.t1.malyugin.tm.dto.model.AbstractWBSDTOModel;

import java.util.List;

@Repository
@NoArgsConstructor
@Scope("prototype")
public abstract class AbstractWBSDTORepository<M extends AbstractWBSDTOModel> extends AbstractUserOwnedDTORepository<M>
        implements IWBSDTORepository<M> {

    @NotNull
    @Override
    public List<M> findAllOrderByCreated(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId ORDER BY m.created";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<M> findAllOrderByName(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId ORDER BY m.name";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<M> findAllOrderByStatus(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId ORDER BY m.status";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

}